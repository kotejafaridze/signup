package com.example.ultimateproject190

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import androidx.appcompat.app.AlertDialog
import com.example.ultimateproject190.databinding.ActivityMainBinding
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity()
{
    private lateinit var  binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        emailFocusListener()
        passwordFocusListener()
        repeatPasswordFocusListener()

        binding.submitButton.setOnClickListener { submitForm() }

    }

    private fun submitForm()
    {
        binding.emailContiner.helperText = validEmail()
        binding.passwordContainer.helperText = validPassword()
        binding.repeatPasswordContainer.helperText = validrepeatPassword()

        val validEmail = binding.emailContiner.helperText == null
        val validPassword = binding.passwordContainer.helperText == null
        val validRepeatPassword = binding.repeatPasswordContainer.helperText == null
        val passwordText = binding.passwordEditText.text.toString()
        val emailText = binding.emailEditText.text.toString()

        if (validEmail && validPassword && validRepeatPassword){
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(emailText ,passwordText)
                .addOnCompleteListener { task -> }
            resetForm()}
        else
            invalidForm()

    }

    private fun invalidForm() {
        var message = ""
        if(binding.emailContiner.helperText != null)
            message += "\n\nEmail:" + binding.emailContiner.helperText
        if(binding.passwordContainer.helperText != null)
            message += "\n\nPassword:" + binding.passwordContainer.helperText
        if(binding.repeatPasswordContainer.helperText != null)
            message +="\n\nRepeat Password:"+ binding.repeatPasswordContainer.helperText

        AlertDialog.Builder(this)
            .setTitle("Invalid Form")
            .setMessage(message)
            .setPositiveButton("Okay"){ _,_ ->
                // araferi
            }
            .show()

    }

    private fun resetForm() {
        var message = "Email:" + binding.emailEditText.text
        message += "\nPassword:" + binding.passwordEditText.text
        message += ""
        AlertDialog.Builder(this)
            .setTitle("Form Submitted")
            .setMessage(message)
            .setPositiveButton("Okay"){ _,_ ->
                binding.emailEditText.text = null
                binding.passwordEditText.text = null
                binding.repeatPasswordEditText.text = null


                binding.emailContiner.helperText = getString(R.string.required)
                binding.passwordContainer.helperText = getString(R.string.required)
                binding.repeatPasswordContainer.helperText = getString(R.string.required)
            }
            .show()

    }

    private fun emailFocusListener()
    {
        binding.emailEditText.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                binding.emailContiner.helperText = validEmail()
            }
        }

    }

    private fun validEmail(): String?
    {
        val emailText = binding.emailEditText.text.toString()
        if(!Patterns.EMAIL_ADDRESS.matcher(emailText).matches())
        {
            return  "Invalid Email Address"
        }

        return null
    }


    private fun passwordFocusListener()
    {
        binding.passwordEditText.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                binding.passwordContainer.helperText = validPassword()
            }
        }

    }

    private fun validPassword(): String?
    {
        val passwordText = binding.passwordEditText.text.toString()
        if (passwordText.length < 9 )
        {
            return "Minimum 9 Character Password"
        }
        if (!passwordText.matches(".*[!@#$%^&*()].*".toRegex()))
        {
            return "Must Contain Minimum 1 Special Character (!@#\$%^&*)"
        }
        if (!passwordText.matches(".*[0123456789].*".toRegex()))
        {
            return "Must Contain Minumum  1 digit"
        }



        return null
    }
    private fun repeatPasswordFocusListener()
    {
        binding.repeatPasswordEditText.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                binding.repeatPasswordContainer.helperText = validrepeatPassword()
            }
        }

    }

    private fun validrepeatPassword(): String?
    {



        val passwordText = binding.passwordEditText.text.toString()
        val repeatPasswordText = binding.repeatPasswordEditText.text.toString()
        if (repeatPasswordText != passwordText)
        {
            return "Passwords Doesn't Match"

        }



        return null
    }
}